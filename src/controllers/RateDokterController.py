from ..model.RateDokterModel import RateDokterModel, RateDokterSchemas
from ..utils.commons_util import custom_response, json_data

rd_schemas = RateDokterSchemas()


class RateDokterController:

    @staticmethod
    def create(req_data):
        error = rd_schemas.validate(req_data)

        if error:
            custom_response(json_data(400, None, error), 400)

        data = rd_schemas.load(req_data)

        try:
            rd = RateDokterModel(data)
            rd.save()
            result = rd_schemas.dump(rd)
            print("Result: ", result)
            response = custom_response(json_data(201, result, None), 201)
        except Exception as e:
            response = custom_response(json_data(400, None, e), 200)

        return response

    @staticmethod
    def get_rate_by_id(id):
        rate = RateDokterModel.get_rating_by_id(id)
        result = rd_schemas.dump(rate, many=True)[0]
        print("RESULT", result)
        return result

    @staticmethod
    def get_all():
        rd = RateDokterModel.get_all()
        result = rd_schemas.dump(rd, many=True)

        return custom_response(json_data(200, result, None), 200)

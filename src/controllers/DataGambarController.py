import cv2

from ..model.DataGambarModel import DataGambarModel, DataGambarSchemas

image_schemas = DataGambarSchemas()

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
UPLOAD_FOLDER = r"D:\Thesis\melanoma-image-classification\src\static"


class DataGambarController:

    @staticmethod
    def save_image(filename, prediction):
        json_file = {
            'gambar': '{}'.format(filename),
            'hasil_prediksi': '{}'.format(prediction[0][0])
        }

        data = image_schemas.load(json_file)

        image = DataGambarModel(data)
        image.save()
        image_schemas.dump(image)

    @staticmethod
    def get_id():
        id_image = 0
        image = DataGambarModel.get_all()
        result = image_schemas.dump(image, many=True)
        for a in range(len(result)):
            id_image = a
        return result[id_image].get("id")

    @staticmethod
    def detail_id(id_gambar):
        detail = DataGambarModel.get_image_by_id(id_gambar)
        result = image_schemas.dump(detail, many=True)[0]
        return result

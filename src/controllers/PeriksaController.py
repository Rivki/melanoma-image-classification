import os

import cv2
import shutil
import tensorflow as tf
from pyfcm import FCMNotification
from werkzeug.utils import secure_filename

from ..controllers.DataGambarController import DataGambarController as gambar_controller
from ..controllers.DokterController import DokterController as dokter_controller
from ..controllers.PasienController import PasienController as pasien_controller
from ..model.PeriksaModel import PeriksaSchemas, PeriksaModel
from ..utils.commons_util import custom_response, json_data

periksa_schemas = PeriksaSchemas()
FCM_SERVER_KEY = os.getenv('FCM_SERVER_KEY')
push_service = FCMNotification(api_key=FCM_SERVER_KEY)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
UPLOAD_FOLDER = r"D:\Thesis\melanoma-image-classification\src\static"
DESTINATION_FOLDER_POSITIF = r"D:\Thesis\melanoma-image-classification\dataset\Melanoma"
DESTINATION_FOLDER_NEGATIF = r"D:\Thesis\melanoma-image-classification\dataset\NotMelanoma"


class PeriksaController:

    @staticmethod
    def __allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def periksa(self, file, keluhan, id_dokter, id_pasien):

        if file.filename == '' or file is None:
            resp = custom_response(json_data(400, None, 'No file selected for uploading'), 200)
            return resp

        if file:
            return self.__upload_data(file, keluhan, id_dokter, id_pasien)
        elif self.__allowed_file(file.filename):
            return self.__upload_data(file, keluhan, id_dokter, id_pasien)

    @staticmethod
    def list_periksa_pasien(id_pasien):
        periksa = PeriksaModel.get_pasien_by_id(id_pasien)
        result = periksa_schemas.dump(periksa, many=True)

        jsons = {
            "data": result
        }

        return custom_response(json_data(200, jsons, None), 200)

    @staticmethod
    def history_periksa_pasien(id_pasien):
        periksa = PeriksaModel.get_history(id_pasien, True)
        result = periksa_schemas.dump(periksa, many=True)

        jsons = {
            "data": result
        }

        return custom_response(json_data(200, jsons, None), 200)

    @staticmethod
    def detail_periksa(id):
        periksa = PeriksaModel.get_by_id(id)
        data_periksa = periksa_schemas.dump(periksa, many=True)[0]
        get_data_dokter = dokter_controller.get_name(data_periksa['dokter_id'])
        data_gambar = gambar_controller.detail_id(data_periksa.get("data_gambar_id"))
        result = data_periksa.copy()
        result.update(get_data_dokter)
        result.update(data_gambar)

        return custom_response(json_data(200, result, None), 200)

    def __upload_data(self, file, keluhan, id_dokter, id_pasien):
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))

        model = tf.keras.models.load_model(r"D:\Thesis\melanoma-image-classification\src\training_model\result.model")

        prediction = model.predict(self.__prepare(os.path.join(UPLOAD_FOLDER, filename)))

        gambar_controller.save_image(filename, prediction)

        req_data = {
            'pasien_id': '{}'.format(id_pasien),
            'data_gambar_id': '{}'.format(gambar_controller.get_id()),
            'keluhan': '{}'.format(keluhan),
            'dokter_id': id_dokter,
            'status': False
        }

        error = periksa_schemas.validate(req_data)

        if error:
            return custom_response(json_data(400, None, error), 200)

        data = periksa_schemas.load(req_data)
        device_id = dokter_controller.get_name(id_dokter)

        periksa = PeriksaModel(data)
        periksa.save()
        result = periksa_schemas.dump(periksa)

        self.push_notification("Pemeriksaan baru telah masuk", "Silakan cek untuk lebih detail", device_id.get('device_id'), "home")

        resp = custom_response(json_data(201, result, None), 201)

        return resp

    @staticmethod
    def __prepare(filepath):
        IMG_SIZE = 60
        image = cv2.imread(filepath)
        img_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        img_rgb = img_rgb / 255.0
        new_array = cv2.resize(img_rgb, (IMG_SIZE, IMG_SIZE))
        return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 3)

    @staticmethod
    def list_periksa_dokter(id_dokter):
        periksa = PeriksaModel.get_periksa_by_id_dokter(id_dokter)
        result = periksa_schemas.dump(periksa, many=True)

        r = {
            "data": result
        }

        return custom_response(json_data(200, r, None), 200)

    def edit_periksa_dokter(self, id, kesimpulan, deskripsi):
        req_data = {
            'kesimpulan': '{}'.format(kesimpulan),
            'deskripsi': '{}'.format(deskripsi),
            'status': True
        }

        error = periksa_schemas.validate(req_data)

        if error:
            return custom_response(json_data(400, None, error), 200)

        data = periksa_schemas.load(req_data)
        id_periksa = int(id)

        try:
            periksa = PeriksaModel.get_by_id_edit(id_periksa)
            periksa.update(data)
            result = periksa_schemas.dump(periksa)

            id_gambar = result.get('data_gambar_id')
            get_nama_file = gambar_controller.detail_id(id_gambar)
            filename_origin = os.path.join(UPLOAD_FOLDER, get_nama_file.get('gambar'))

            if kesimpulan == "positif":
                filename_destination_positif = os.path.join(DESTINATION_FOLDER_POSITIF, get_nama_file.get('gambar'))
                shutil.copyfile(filename_origin, filename_destination_positif)
            else:
                filename_destination_negatif = os.path.join(DESTINATION_FOLDER_NEGATIF, get_nama_file.get('gambar'))
                shutil.copyfile(filename_origin, filename_destination_negatif)

            device_id = pasien_controller.get_one_user(result.get('pasien_id'))
            self.push_notification("Data sudah diperiksa", "Data pemeriksaan anda sudah diperiksa, silakan buka aplikasi untuk melihat detailnya", device_id['device_id'], "history")
            response = custom_response(json_data(200, result, None), 200)
        except Exception as e:
            response = custom_response(json_data(400, None, "Data tidak ditemukan"), 200)
        return response

    @staticmethod
    def detail_periksa_dokter(id):
        periksa = PeriksaModel.get_by_id(id)
        data_periksa = periksa_schemas.dump(periksa, many=True)[0]
        get_data_pasien = pasien_controller.get_one_user(data_periksa['pasien_id'])
        data_gambar = gambar_controller.detail_id(data_periksa.get("data_gambar_id"))
        result = data_periksa.copy()
        result.update(get_data_pasien)
        result.update(data_gambar)

        return custom_response(json_data(200, result, None), 200)

    @staticmethod
    def history_periksa_dokter(id_dokter):

        periksa = PeriksaModel.get_history(id_dokter, False)
        result = periksa_schemas.dump(periksa, many=True)
        return result

    @staticmethod
    def push_notification(title, message, device_id, screen):
        registration_id = device_id
        data_message = {
            "message": message,
            "screen": screen
        }
        push_service.notify_single_device(registration_id=registration_id, data_message=data_message,
                                          message_title=title)

import os

from flask import Blueprint
from werkzeug.utils import secure_filename

from ..controllers.RateDokterController import RateDokterController as rate_controller
from ..model.DokterModel import DokterModel, DokterSchemas
from ..shared.AuthenticationDokter import AuthDokter
from ..utils.commons_util import custom_response, json_data

dokter_api = Blueprint('dokter_api', __name__)
dokter_schemas = DokterSchemas()

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
UPLOAD_FOLDER = r"D:\Thesis\melanoma-image-classification\src\static"


class DokterController:

    @staticmethod
    def __allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def create(self, req_data):
        data = dokter_schemas.load(req_data)

        dokter_in_db = DokterModel.get_user_by_email(data.get('email'))
        if dokter_in_db:
            return custom_response(json_data(400, None, 'Email sudah dipakai, silakan masukan email yang lain'), 200)

        return custom_response(json_data(201, self.save_data(data, 'create'), None), 201)

    @staticmethod
    def get_all():
        dokters = DokterModel.get_all()
        result = dokter_schemas.dump(dokters, many=True)
        r = {"data": result}

        return custom_response(json_data(200, r, None), 200)

    def update(self, dokter_id, nama, jk, no_hp, ttl, tanggal_lahir, umur, file, alamat):
        filename = secure_filename(file.filename)

        data = {
            'nama': nama,
            'jk': jk,
            'no_hp': no_hp,
            'tempat_tanggal_lahir': ttl,
            'tanggal_lahir': tanggal_lahir,
            'umur': umur,
            'gambar': filename,
            'alamat': alamat
        }

        data = dokter_schemas.load(data, partial=True)

        if file.filename == '':
            response = custom_response(json_data(200, self.save_data(data, "update", dokter_id), None), 200)
        elif file:
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            response = custom_response(json_data(200, self.save_data(data, "update", dokter_id), None), 200)
        elif self.__allowed_file(file.filename):
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            response = custom_response(json_data(200, self.save_data(data, "update", dokter_id), None), 200)
        else:
            response = custom_response(json_data(400, None, 'File not allowed'), 200)

        return response

    @staticmethod
    def delete(dokter_id):
        try:
            dokters = DokterModel.get_by_id(dokter_id)
            dokters.delete()
            response = custom_response(json_data(200, 'Data sudah terhapus', None), 200)
        except Exception as e:
            response = custom_response(json_data(400, "Data tidak ada", None), 200)
        return response

    @staticmethod
    def login(req_data):
        json_datas = {}

        data = dokter_schemas.load(req_data, partial=True)

        dokter = DokterModel.get_user_by_email(data.get('email'))

        if not data.get('email') or not data.get('password'):
            return custom_response(json_data(401, None, 'Email atau password kosong'), 200)

        if not dokter:
            return custom_response(json_data(401, None, 'Email tidak terdaftar'), 200)

        if not dokter.check_hash(data.get('password')):
            return custom_response(json_data(401, None, 'Password salah'), 200)

        result = dokter_schemas.dump(dokter)
        data_dokter = DokterModel.get_name_dokter(result.get('id'))
        result_dokter = dokter_schemas.dump(data_dokter, many=True)[0]
        token = AuthDokter.generate_token(result.get('id'))
        DokterModel.update_device_id(data.get('device_id'), result.get('id'))


        data = result_dokter.copy()
        t = {'token': token}
        data.update(t)

        json_datas['status'] = 200
        json_datas['result'] = data
        json_datas['error'] = None

        return custom_response(json_datas, 200)

    @staticmethod
    def get_name(id):
        name = DokterModel.get_data_dokter(id)
        data_dokter = dokter_schemas.dump(name, many=True)[0]
        rate_dokter = rate_controller.get_rate_by_id(id)
        result = data_dokter.copy()
        r = {"rate": rate_dokter.get('rate')}
        result.update(r)
        return result

    @staticmethod
    def get_phone_number_doctor(id):
        phone_number = DokterModel.get_phone_number(id)
        result = dokter_schemas.dump(phone_number, many=True)[0]
        return result

    @staticmethod
    def save_data(data, proses, id_dokter=0):
        if proses is "create":
            dokter = DokterModel(data)
            dokter.save()
            dokter_data = dokter_schemas.dump(dokter)

            d = dokter_data.copy()
            token = AuthDokter.generate_token(dokter_data.get('id'))
            t = {'jwt_token': token}
            d.update(t)
            return d
        elif proses is 'update':
            dokters = DokterModel.get_by_id(id_dokter)
            dokters.update(data)
            result = dokter_schemas.dump(dokters)
            return result

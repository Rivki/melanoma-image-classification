import os

from werkzeug.utils import secure_filename

from ..model.PasienModel import PasienModel, PasienSchemas
from ..shared.AuthenticationPasien import AuthPasien
from ..utils.commons_util import *

pasien_schemas = PasienSchemas()

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
UPLOAD_FOLDER = r"D:\Thesis\melanoma-image-classification\src\static"


class PasienController:

    @staticmethod
    def __allowed_file(filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def create(self, req_data):
        error = pasien_schemas.validate(req_data)
        if error:
            custom_response(json_data(400, None, error), 200)

        data = pasien_schemas.load(req_data)
        pasien_email = PasienModel.get_user_by_email(data.get('email'))

        if pasien_email:
            return custom_response(json_data(400, None, 'Email sudah dipakai, silakan masukan email yang lain'), 200)

        return custom_response(json_data(201, self.__save_data(data, "create"), None), 201)

    @staticmethod
    def get_all():
        try:
            pasiens = PasienModel.get_all()
            result = pasien_schemas.dump(pasiens, many=True)
        except Exception as e:
            return custom_response(json_data(400, None, "Data tidak ada"), 200)

        return custom_response(json_data(200, result, None), 200)

    def update(self, id, nama, jk, no_hp, ttl, tanggal_lahir, umur, riwayat_penyakit, file, alamat):
        filename = secure_filename(file.filename)

        data = {
            'nama': nama,
            'jk': jk,
            'no_hp': no_hp,
            'tempat_tanggal_lahir': ttl,
            'tanggal_lahir': tanggal_lahir,
            'umur': umur,
            'riwayat_penyakit': riwayat_penyakit,
            'gambar': filename,
            'alamat': alamat
        }

        data = pasien_schemas.load(data, partial=True)

        try:
            if file.filename == '':
                response = custom_response(json_data(200, self.__save_data(data, "update", id), None), 200)
            elif file:
                file.save(os.path.join(UPLOAD_FOLDER, filename))
                response = custom_response(json_data(200, self.__save_data(data, "update", id), None), 200)
            elif self.__allowed_file(file.filename):
                file.save(os.path.join(UPLOAD_FOLDER, filename))
                response = custom_response(json_data(200, self.__save_data(data, "update", id), None), 200)
            else:
                response = custom_response(json_data(400, None, 'File not allowed'), 200)
        except Exception as e:
            response = custom_response(json_data(400, None, e), 200)
        return response

    @staticmethod
    def delete(pasien_id):
        try:
            pasiens = PasienModel.get_by_id(pasien_id)
            pasiens.delete()
            response = custom_response(json_data(200, "Data sudah terhapus", None), 200)
        except Exception as e:
            response = custom_response(json_data(400, None, "Data tidak ada"), 200)
        return response

    @staticmethod
    def get_one_user(id):
        pasien = PasienModel.get_user_by_id(id)
        result = pasien_schemas.dump(pasien, many=True)[0]
        return result

    @staticmethod
    def login(req_data):
        result_data = {}

        data = pasien_schemas.load(req_data, partial=True)
        error = pasien_schemas.validate(req_data, partial=True)

        if error:
            return custom_response(error, 404)

        pasien = PasienModel.get_user_by_email(data.get('email'))

        if not data.get('email') or not data.get('password'):
            return custom_response(json_data(400, None, 'Email atau password kosong'), 200)

        if not pasien:
            return custom_response(json_data(400, None, 'Email tidak terdaftar'), 200)

        if not pasien.check_hash(data.get('password')):
            return custom_response(json_data(400, None, 'Password salah'), 200)

        result = pasien_schemas.dump(pasien)
        get_data_pasien = PasienModel.get_user_by_id(result.get('id'))
        rlt_data_pasien = pasien_schemas.dump(get_data_pasien, many=True)[0]
        PasienModel.update_device_id(data.get('device_id'), result.get('id'))

        token = AuthPasien.generate_token(result.get('id'))
        d = rlt_data_pasien.copy()
        t = {'jwt_token': token}
        d.update(t)

        result_data['status'] = 200
        result_data['result'] = d
        result_data['error'] = None

        return custom_response(result_data, 200)

    @staticmethod
    def __save_data(data, proses, id_pasien=0):
        if proses is "create":
            pasiens = PasienModel(data)
            pasiens.save()
            data_pasien = pasien_schemas.dump(pasiens)
            result = data_pasien.copy()
            token = AuthPasien.generate_token(data_pasien.get("id"))
            t = {'token': token}
            result.update(t)
            return result
        elif proses is "update":
            pasiens = PasienModel.get_by_id(id_pasien)
            pasiens.update(data)
            result = pasien_schemas.dump(pasiens)
            return result

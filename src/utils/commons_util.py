from flask import Response, json, request


def json_data(status, data, error):
    json_res = {
        'status': status,
        'result': data,
        'error': error
    }
    return json_res


def custom_response(res, status_code):
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code
    )


def check_credentials(data, data_user):
    if not data.get('email') or not data.get('password'):
        return custom_response(json_data(400, None, 'Email atau password kosong'), 400)

    if not data_user:
        return custom_response(json_data(400, None, 'Invalid credentials'), 400)

    if not data_user.check_hash(data.get('password')):
        return custom_response(json_data(400, None, 'Invalid credentials'), 400)

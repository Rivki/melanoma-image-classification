from marshmallow import fields, Schema
import datetime
from enum import Enum
from .RateDokterModel import RateDokterSchemas
from ..app import bcrypt
from . import db


class Gender(Enum):
    laki_laki = "laki-laki"
    perempuan = "perempuan"


class DokterModel(db.Model):
    __tablename__ = 'dokters'

    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(128), nullable=False)
    jk = db.Column(db.Enum(Gender))
    no_hp = db.Column(db.String(13), nullable=True)
    email = db.Column(db.String(50), nullable=True)
    password = db.Column(db.String(128), nullable=False)
    device_id = db.Column(db.String(255), nullable=True)
    tempat_tanggal_lahir = db.Column(db.String(100), nullable=True)
    tanggal_lahir = db.Column(db.String(100), nullable=True)
    umur = db.Column(db.Integer(), nullable=True)
    gambar = db.Column(db.String(128), nullable=True)
    alamat = db.Column(db.String(128), nullable=True)

    create_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    periksa = db.relationship('PeriksaModel', backref='periksas', lazy=True)
    rate_dokter = db.relationship('RateDokterModel', backref='rate_dokters', lazy=True)

    def __init__(self, data):
        self.nama = data.get('nama')
        self.jk = data.get('jk')
        self.no_hp = data.get('no_hp')
        self.email = data.get('email')
        self.tempat_tanggal_lahir = data.get('tempat_tanggal_lahir')
        self.tanggal_lahir = data.get('tanggal_lahir')
        self.umur = data.get('umur')
        self.riwayat_penyakit = data.get('riwayat_penyakit')
        self.gambar = data.get('gambar')
        self.password = data.get('password')
        self.device_id = data.get('device_id')
        self.alamat = data.get('alamat')
        self.create_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            if key == 'password':
                self.password = self.__generate_hash(data.get('password'))
            setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.__generate_hash(self.password), password)

    @staticmethod
    def get_user_by_email(value):
        return DokterModel.query.filter_by(email=value).first()

    @staticmethod
    def get_all():
        return DokterModel.query.all()

    @staticmethod
    def get_by_id(id):
        return DokterModel.query.get(id)

    @staticmethod
    def get_data_dokter(id):
        return db.engine.execute('SELECT id, nama, alamat, email, no_hp, tempat_tanggal_lahir, tanggal_lahir, umur, gambar, jk, device_id FROM dokters where id={}'.format(id))

    @staticmethod
    def get_name_dokter(id):
        return db.engine.execute('SELECT id, nama, alamat, email, no_hp FROM dokters where id={}'.format(id))

    @staticmethod
    def update_device_id(device_id, id_pasien):
        return db.engine.execute("UPDATE dokters set device_id='{}' where id = {}".format(device_id, id_pasien))

    def __repo(self):
        return '<id {}>'.format(self.id)


class DokterSchemas(Schema):
    id = fields.Int(dump_only=True)
    nama = fields.Str(required=True)
    jk = fields.String(description='The object type', enum=Gender.member_names_)
    no_hp = fields.Str(required=False)
    email = fields.Str(required=False)
    tempat_tanggal_lahir = fields.Str(required=False)
    tanggal_lahir = fields.Str(required=False)
    umur = fields.Int(required=False)
    gambar = fields.Str(required=False)
    device_id = fields.Str(required=False)
    password = fields.Str(required=True)
    alamat = fields.Str(required=False)
    create_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)
    rate_dokter = fields.Nested(RateDokterSchemas, many=True)

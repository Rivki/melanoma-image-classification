from marshmallow import fields, Schema
import datetime
from .PeriksaModel import PeriksaSchemas
from . import db


class DataGambarModel(db.Model):
    __tablename__ = 'data_gambars'

    id = db.Column(db.Integer, primary_key=True)
    gambar = db.Column(db.String(255), nullable=False)
    hasil_prediksi = db.Column(db.String(255), nullable=True)
    create_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    periksa = db.relationship('PeriksaModel', backref='periksa_gambar', lazy=True)

    def __init__(self, data):
        self.gambar = data.get("gambar")
        self.hasil_prediksi = data.get("hasil_prediksi")
        self.create_at = datetime.datetime.utcnow()
        self.update_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return DataGambarModel.query.all()

    @staticmethod
    def get_by_id(id):
        return DataGambarModel.query.get(id)

    @staticmethod
    def get_image_by_id(id):
        conn = db.engine.connect()
        return conn.execute("SELECT gambar, hasil_prediksi FROM data_gambars where id='{}'".format(id))

    def __repr__(self):
        return '<id {}>'.format(self.id)


class DataGambarSchemas(Schema):
    id = fields.Int(dump_only=True)
    gambar = fields.Str(required=True)
    hasil_prediksi = fields.Str(required=False)
    create_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)
    periksa = fields.Nested(PeriksaSchemas, many=True)

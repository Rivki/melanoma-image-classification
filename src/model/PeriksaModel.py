import datetime
from marshmallow import fields, Schema
from . import db


class PeriksaModel(db.Model):
    __tablename__ = 'periksas'

    id = db.Column(db.Integer, primary_key=True)
    kesimpulan = db.Column(db.String, nullable=True)
    deskripsi = db.Column(db.String, nullable=True)
    keluhan = db.Column(db.String, nullable=False)
    status = db.Column(db.Boolean, nullable=False, default=False)
    create_at = db.Column(db.DateTime)
    update_at = db.Column(db.DateTime)
    data_gambar_id = db.Column(db.Integer, db.ForeignKey('data_gambars.id'), nullable=False)
    pasien_id = db.Column(db.Integer, db.ForeignKey('pasiens.id'), nullable=False)
    dokter_id = db.Column(db.Integer, db.ForeignKey('dokters.id'), nullable=True)

    def __init__(self, data):
        self.kesimpulan = data.get("kesimpulan")
        self.deskripsi = data.get("deskripsi")
        self.data_gambar_id = data.get("data_gambar_id")
        self.status = data.get("status")
        self.keluhan = data.get("keluhan")
        self.pasien_id = data.get("pasien_id")
        self.dokter_id = data.get("dokter_id")
        self.create_at = datetime.datetime.utcnow()
        self.update_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        self.update_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return PeriksaModel.query.all()

    @staticmethod
    def get_all_pasien():
        conn = db.engine.connect()
        sql = conn.execute('SELECT * FROM periksas where status = false')
        return sql

    @staticmethod
    def get_pasien_by_id(id):
        conn = db.engine.connect()
        sql = conn.execute('select * from periksas where pasien_id = {} and status = false'.format(id))
        return sql

    @staticmethod
    def get_history(id, type):
        if type:
            sql = db.engine.execute('select * from periksas where pasien_id = {} and status = true'.format(id))
        else:
            sql = db.engine.execute('select * from periksas where dokter_id = {} and status = true'.format(id))
        return sql

    @staticmethod
    def get_periksa_by_id_dokter(id):
        return db.engine.execute('select * from periksas where dokter_id = {} and status = false'.format(id))

    @staticmethod
    def get_by_id(id):
        return db.engine.execute('SELECT id, kesimpulan, status, deskripsi, keluhan, pasien_id, dokter_id, data_gambar_id, create_at FROM periksas WHERE id={}'.format(id))

    @staticmethod
    def get_by_id_edit(id):
        return PeriksaModel.query.get(id)

    def __repr__(self):
        return '<id {}>'.format(self.id)


class PeriksaSchemas(Schema):
    id = fields.Int(dump_only=True)
    kesimpulan = fields.Str(required=False)
    deskripsi = fields.Str(required=False)
    status = fields.Boolean(required=True, default=False)
    keluhan = fields.String(required=False)
    create_at = fields.DateTime(dump_only=True)
    update_at = fields.DateTime(dump_only=True)
    pasien_id = fields.Int(required=False)
    data_gambar_id = fields.Int(required=False)
    dokter_id = fields.Int(required=False)

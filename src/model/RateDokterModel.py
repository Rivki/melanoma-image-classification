from marshmallow import fields, Schema
import datetime
from . import db


class RateDokterModel(db.Model):
    __tablename__ = "rate_dokters"

    id = db.Column(db.Integer, primary_key=True)
    rate = db.Column(db.Integer, nullable=True)
    note = db.Column(db.String, nullable=True)
    create_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    dokter_id = db.Column(db.Integer, db.ForeignKey('dokters.id'), nullable=False)

    def __init__(self, data):
        self.rate = data.get("rate")
        self.note = data.get("note")
        self.dokter_id = data.get("dokter_id")
        self.create_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return RateDokterModel.query.all()

    @staticmethod
    def get_rating_by_id(id):
        return db.engine.execute("select avg(rate) as rate from rate_dokters where dokter_id={}".format(id))

    def __repr__(self):
        return '<id {}>'.format(self.id)


class RateDokterSchemas(Schema):
    id = fields.Int(dump_only=True)
    rate = fields.Float(required=False)
    note = fields.Str(required=False)
    dokter_id = fields.Int(required=False)
    create_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)

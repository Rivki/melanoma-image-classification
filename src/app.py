from flask import Flask

from .config import app_config
from .model import db, bcrypt

from .views.DokterViews import dokter_api as dokter_blueprint
from .views.PasienViews import pasien_api as pasien_blueprint
from .views.RateDokterViews import rd_api as rating_blueprint
from .views.PeriksaViews import periksa_api as periksa_blueprint

UPLOAD_FOLDER = r"D:\Thesis\melanoma-image-classification\upload"


def create_app(env_name):
    app = Flask(__name__)

    app.config.from_object(app_config[env_name])
    app.config['UPLOAD_FILE'] = UPLOAD_FOLDER
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 102

    bcrypt.init_app(app)

    db.init_app(app)

    app.register_blueprint(dokter_blueprint, url_prefix='/api/v1/dokter')
    app.register_blueprint(pasien_blueprint, url_prefix='/api/v1/pasien')
    app.register_blueprint(rating_blueprint, url_prefix='/api/v1/rating')
    app.register_blueprint(periksa_blueprint, url_prefix='/api/v1/periksa')

    @app.route('/', methods=['GET'])
    def index():
        return 'Melanoma Classifier Webservice'

    return app

import os

from flask import request, Blueprint, g
from pyfcm import FCMNotification

from ..controllers.DokterController import DokterController
from ..shared.AuthenticationDokter import AuthDokter
from ..utils.commons_util import custom_response, json_data

dokter_api = Blueprint('dokter_api', __name__)
controller = DokterController()


@dokter_api.route('/add', methods=['POST'])
def create_user_doctor():
    req_data = request.get_json()
    return controller.create(req_data)


@dokter_api.route('/all', methods=['GET'])
def get_all():
    return controller.get_all()


@dokter_api.route('/edit', methods=['PUT'])
@AuthDokter.auth_required
def edit_data_dokter():
    file = request.files['file_upload']
    nama = request.form['nama']
    jk = request.form['jenis_kelamin']
    no_telepon = request.form['no_telepon']
    ttl = request.form['tempat_tanggal_lahir']
    tanggal_lahir = request.form['tanggal_lahir']
    umur = request.form['umur']
    alamat = request.form['alamat']

    result = controller.update(g.user.get('id'), nama, jk, no_telepon, ttl, tanggal_lahir, umur, file, alamat)

    return result


@dokter_api.route('/delete/<int:dokter_id>', methods=['DELETE'])
def delete_data_dokter(dokter_id):
    return controller.delete(dokter_id)


@dokter_api.route('/login', methods=['POST'])
def login():
    req_data = request.get_json()
    return controller.login(req_data)


@dokter_api.route('/get_dokter/id=<int:dokter_id>', methods=['GET'])
def get_dokter(dokter_id):
    result = controller.get_name(dokter_id)
    return custom_response(json_data(200, result, None), 200)


@dokter_api.route('/user', methods=['GET'])
@AuthDokter.auth_required
def get_user():
    result = controller.get_name(g.user.get('id'))
    return custom_response(json_data(200, result, None), 200)

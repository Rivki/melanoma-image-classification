import os

from flask import request, g, Blueprint
from ..controllers.PasienController import PasienController
from ..shared.AuthenticationPasien import AuthPasien
from ..utils.commons_util import custom_response, json_data
from pyfcm import FCMNotification

pasien_api = Blueprint('pasien_api', __name__)
pasien_controller = PasienController()


@pasien_api.route('/add', methods=['POST'])
def create_pasien():
    req_data = request.get_json()
    result = pasien_controller.create(req_data)
    return result


@pasien_api.route('/all', methods=['GET'])
def get_all_pasien():
    return pasien_controller.get_all()


@pasien_api.route('/edit', methods=['PUT'])
@AuthPasien.auth_required
def edit_pasien():
    file = request.files['file_upload']
    nama = request.form['nama']
    jk = request.form['jenis_kelamin']
    no_telepon = request.form['no_telepon']
    ttl = request.form['tempat_tanggal_lahir']
    tanggal_lahir = request.form['tanggal_lahir']
    umur = request.form['umur']
    riwayat_penyakit = request.form['riwayat_penyakit']
    alamat = request.form['alamat']

    result = pasien_controller.update(g.user.get('id'), nama, jk, no_telepon, ttl, tanggal_lahir, umur,
                                      riwayat_penyakit, file, alamat)
    return result


@pasien_api.route('/delete/<int:pasien_id>', methods=['DELETE'])
@AuthPasien.auth_required
def delete_pasien(pasien_id):
    return pasien_controller.delete(pasien_id)


@pasien_api.route('/user', methods=['GET'])
@AuthPasien.auth_required
def get_user():
    result = pasien_controller.get_one_user(g.user.get("id"))
    return custom_response(json_data(200, result, None), 200)


@pasien_api.route('/login', methods=['POST'])
def login():
    req_data = request.get_json()
    return pasien_controller.login(req_data)

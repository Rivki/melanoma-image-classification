from flask import request, g, Blueprint
from ..shared.AuthenticationPasien import AuthPasien as AuthPasien
from ..shared.AuthenticationDokter import AuthDokter
from ..utils.commons_util import custom_response, json_data
from ..controllers.PeriksaController import PeriksaController
from ..training_model.training_data import TrainingData

periksa_api = Blueprint('periksa_api', __name__)
periksa_controller = PeriksaController()


@periksa_api.route('/check', methods=['POST'])
@AuthPasien.auth_required
def periksa():
    if 'file_upload' not in request.files:
        resp = custom_response(json_data(400, None, 'No file part in the request'), 400)
        return resp
    file = request.files['file_upload']
    keluhan = request.form['keluhan']
    id_dokter = request.form['id_dokter']
    return periksa_controller.periksa(file, keluhan, id_dokter, g.user.get('id'))


@periksa_api.route('/list_periksa_pasien', methods=['GET'])
@AuthPasien.auth_required
def list_periksa():
    return periksa_controller.list_periksa_pasien(g.user.get('id'))


@periksa_api.route('/history_periksa_pasien', methods=['GET'])
@AuthPasien.auth_required
def history_periksa_pasien():
    return periksa_controller.history_periksa_pasien(g.user.get('id'))


@periksa_api.route('/detail_pasien/id=<int:id>', methods=['GET'])
@AuthPasien.auth_required
def detail_pasien(id):
    return periksa_controller.detail_periksa(id)


@periksa_api.route('/list_periksa_dokter', methods=['GET'])
@AuthDokter.auth_required
def get_list_periksa_dokter():
    return periksa_controller.list_periksa_dokter(g.user.get('id'))


@periksa_api.route('/history_periksa_dokter', methods=['GET'])
@AuthDokter.auth_required
def get_history_periksa_dokter():
    periksa = periksa_controller.history_periksa_dokter(g.user.get('id'))
    result = {'data': periksa}
    return custom_response(json_data(200, result, None), 200)


@periksa_api.route('/detail_dokter/id=<int:id>', methods=['GET'])
@AuthDokter.auth_required
def get_detail_dokter(id):
    return periksa_controller.detail_periksa_dokter(id)


@periksa_api.route('/edit_periksa', methods=['PUT'])
@AuthDokter.auth_required
def edit_periksa():
    id_periksa = request.form['id_periksa']
    kesimpulan = request.form['kesimpulan']
    deskripsi = request.form['deskripsi']
    return periksa_controller.edit_periksa_dokter(id_periksa, kesimpulan, deskripsi)


@periksa_api.route('/training_data', methods=['GET'])
def trainig_data():
    TrainingData.training_data()
    return custom_response(json_data(200, 'training data sukses', None), 200)

from flask import request, g, Blueprint, json, Response
from ..controllers.RateDokterController import RateDokterController as controller
from ..shared.AuthenticationPasien import AuthPasien

rd_api = Blueprint('rating_api', __name__)


@rd_api.route('/rate', methods=['POST'])
@AuthPasien.auth_required
def rate_dokter():
    req_data = request.get_json()
    return controller.create(req_data)


@rd_api.route('/all', methods=['GET'])
def get_all():
    return controller.get_all()
from functools import wraps

import jwt
import os
import datetime
from flask import json, g, request
from werkzeug import Response

from ..model.PasienModel import PasienModel
from ..utils.commons_util import json_data, custom_response


class AuthPasien:

    @staticmethod
    def generate_token(id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
                'iat': datetime.datetime.utcnow(),
                'sub': id
            }
            return jwt.encode(
                payload,
                os.getenv('JWT_SECRET'),
                'HS256'
            ).decode("utf-8")
        except Exception as e:
            return Response(
                mimetype="application/json",
                response=json.dumps({'error': 'error in generating user token'}),
                status=400
            )

    @staticmethod
    def decode_token(token):
        re = {'data': {}, 'error': {}}
        try:
            payload = jwt.decode(token, os.getenv('JWT_SECRET'))
            re['data'] = {'id': payload['sub']}
            return re
        except jwt.ExpiredSignatureError as e1:
            re['error'] = {'message': 'token expired, please login again'}
            return re
        except jwt.InvalidTokenError:
            re['error'] = {'message': 'Invalid token, please try again with a new token'}
            return re

    @staticmethod
    def auth_required(func):

        @wraps(func)
        def decorated_auth(*args, **kwargs):
            if 'api-token' not in request.headers:
                response = json_data(401, None, 'Authentication token is not available, please login to get one')
                return custom_response(response, 401)
            token = request.headers.get('api-token')
            data = AuthPasien.decode_token(token)
            if data['error']:
                response = json_data(400, None, data['error'])
                return custom_response(response, 400)

            id = data['data']['id']
            check_user = PasienModel.get_by_id(id)

            if not check_user:
                response = json_data(400, None, 'user does not exist, invalid token')
                return custom_response(response, 400)
            g.user = {'id': id}
            return func(*args, **kwargs)

        return decorated_auth

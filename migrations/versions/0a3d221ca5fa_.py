"""empty message

Revision ID: 0a3d221ca5fa
Revises: 5abd7c3e297a
Create Date: 2020-05-30 16:23:18.482349

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0a3d221ca5fa'
down_revision = '5abd7c3e297a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('pasiens', sa.Column('gambar', sa.String(length=128), nullable=True))
    op.add_column('pasiens', sa.Column('riwayat_penyakit', sa.String(length=100), nullable=True))
    op.add_column('pasiens', sa.Column('tanggal_lahir', sa.String(length=100), nullable=True))
    op.add_column('pasiens', sa.Column('umur', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('pasiens', 'umur')
    op.drop_column('pasiens', 'tanggal_lahir')
    op.drop_column('pasiens', 'riwayat_penyakit')
    op.drop_column('pasiens', 'gambar')
    # ### end Alembic commands ###

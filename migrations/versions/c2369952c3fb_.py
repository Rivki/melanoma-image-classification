"""empty message

Revision ID: c2369952c3fb
Revises: 1913edba0ea1
Create Date: 2020-06-01 14:02:48.588141

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c2369952c3fb'
down_revision = '1913edba0ea1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('rate_dokters', sa.Column('note', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('rate_dokters', 'note')
    # ### end Alembic commands ###

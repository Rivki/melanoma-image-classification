# MELANOMA CLASSIFER WEBSERVICE

### Minimal Requirement
* Python version 3.7
* Postgresql
* virtualvenv
* pipenv (optional)

### Minimal Hardware Requirement
* Processor H series (intel core i5 7th generation/AMD Ryzen 2nd series)
* VGA NVIDIA (GTX Version)
* RAM 8 gb

### Use 
* create database in postgresql name melanoma_classifier
* edit file .env and change user name and password in DATABASE_URL
* create venv and install `requirement.txt`
* run `python manage.py db migrate`
* run `python manage.py db upgrade`
* run `python run.py`

### Documentation
* [FLASK](https://flask.palletsprojects.com/en/1.1.x/)
* [TENSORFLOW](https://www.tensorflow.org/)
* [FIREBASE FCM](https://firebase.google.com/docs/cloud-messaging)
* [OPENCV](https://opencv.org/)
* [PYFCM](https://github.com/olucurious/pyfcm)
* [FLASK SQL ALCHEMY](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)

## HAPPY CODING! :D